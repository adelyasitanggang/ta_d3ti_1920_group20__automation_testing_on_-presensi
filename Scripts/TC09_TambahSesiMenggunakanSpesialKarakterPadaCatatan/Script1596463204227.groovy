import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\adely\\Katalon Studio\\Mobile Test\\androidapp\\presensi-testing.apk', true)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - Username'), 'ike.fitri', 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - Password'), 'bangB0telG4nteng', 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.TextView0 - LOGIN'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.TextView0 - Aljabar Linier'),0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.view.ViewGroup0'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.Spinner0'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.CheckedTextView0 - Reguler'),0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.Spinner0 (1)'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.CheckedTextView0 - Teori'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.TextView0 - Pilih Lokasi'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.CheckedTextView0 - GD512'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.TextView0 - Waktu Mulai'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.ImageButton0'), 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - 16'), '08', 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - 52'), '00', 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.Button0 - OK'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.TextView0 - Waktu Selesai'), 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.ImageButton0'), 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - 16'), '09', 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - 52'), '00', 0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.Button0 - OK'), 0)
Mobile.setText(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.widget.EditText0 - Catatan'), '@#$%^&*',0)
Mobile.tap(findTestObject('TambahSesiMenggunakanSpesialKarakterPadaCatatan/android.view.ViewGroup0 (1)'), 0)
Mobile.closeApplication()









