import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\adely\\Katalon Studio\\Mobile Test\\androidapp\\presensi-testing.apk', true)

Mobile.setText(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.EditText0 - Username'), 'ike.fitri', 0)

Mobile.setText(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.EditText0 - Password'), 'bangB0telG4nteng', 
    0)

Mobile.tap(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.TextView0 - LOGIN'), 0)

Mobile.tap(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.TextView0 - Aljabar Linier'), 0)

Mobile.tap(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.TextView0 - MODE MAHASISWA'), 0)

Mobile.tap(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.TextView0 - MODE PENGAJAR'), 0)

Mobile.setText(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.EditText0 - Password (1)'), 'adelyassss1123', 
    0)

Mobile.tap(findTestObject('ModePengajarDenganPassMhsSalah/android.widget.TextView0 - AUTENTIKASI'), 0)

Mobile.closeApplication()





